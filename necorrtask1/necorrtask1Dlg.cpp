
// necorrtask1Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "necorrtask1.h"
#include "necorrtask1Dlg.h"
#include "afxdialogex.h"

#include <iostream>
#include <math.h>
#include <time.h>
#include <random>
#include <fstream>

#include <conio.h>


#define DOTS(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) // ������ �������� ��������� ��� ������� �������
#define DOTSIMP(x,y) (xpimp*((x)-xminimp)),(ypimp*((y)-ymaximp)) // ������ �������� ��������� ��� ������� ������+���
#define DOTSSVERT(x,y) (xpsvert*((x)-xminsvert)),(ypsvert*((y)-ymaxsvert)) // ������ �������� ��������� ��� ������� �������

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;


// ���������� ���� Cnecorrtask1Dlg



Cnecorrtask1Dlg::Cnecorrtask1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cnecorrtask1Dlg::IDD, pParent)
	, A1_sign(5)
	, A2_sign(2)
	, A3_sign(3.5)
	, A1_imp(1)
	, A2_imp(1)
	, dis1_sign(4)
	, dis2_sign(3)
	, dis3_sign(5)
	, t01_sign(6)
	, t02_sign(30)
	, t03_sign(42)
	, dis1_imp(5)
	, dis2_imp(5)
	, t01_imp(0)
	, t02_imp(50)
	, t_sign(50)
	, error(_T(""))
	, percent_shum(2)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cnecorrtask1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, A1_sign);
	DDX_Text(pDX, IDC_EDIT2, A2_sign);
	DDX_Text(pDX, IDC_EDIT3, A3_sign);
	DDX_Text(pDX, IDC_EDIT10, A1_imp);
	DDX_Text(pDX, IDC_EDIT11, A2_imp);
	DDX_Text(pDX, IDC_EDIT4, dis1_sign);
	DDX_Text(pDX, IDC_EDIT5, dis2_sign);
	DDX_Text(pDX, IDC_EDIT6, dis3_sign);
	DDX_Text(pDX, IDC_EDIT7, t01_sign);
	DDX_Text(pDX, IDC_EDIT8, t02_sign);
	DDX_Text(pDX, IDC_EDIT9, t03_sign);
	DDX_Text(pDX, IDC_EDIT13, dis1_imp);
	DDX_Text(pDX, IDC_EDIT14, dis2_imp);
	DDX_Text(pDX, IDC_EDIT16, t01_imp);
	DDX_Text(pDX, IDC_EDIT17, t02_imp);
	DDX_Text(pDX, IDC_EDIT19, t_sign);
	DDX_Text(pDX, IDC_STATIC_ERR, error);
	DDX_Text(pDX, IDC_EDIT20, percent_shum);
}

BEGIN_MESSAGE_MAP(Cnecorrtask1Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &Cnecorrtask1Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Cnecorrtask1Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &Cnecorrtask1Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &Cnecorrtask1Dlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// ����������� ��������� Cnecorrtask1Dlg

BOOL Cnecorrtask1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	//���  ��������
	PicWnd = GetDlgItem(IDC_SIGNAL);
	PicDc = PicWnd->GetDC();
	PicWnd->GetClientRect(&Pic);

	PicWndImp = GetDlgItem(IDC_IMPULS);
	PicDcImp = PicWndImp->GetDC();
	PicWndImp->GetClientRect(&PicImp);

	PicWndSvert = GetDlgItem(IDC_SVERTKA);
	PicDcSvert = PicWndSvert->GetDC();
	PicWndSvert->GetClientRect(&PicSvert);

	// �����
	setka_pen.CreatePen(		//��� �����
		PS_DOT,					//����������
		1,						//������� 1 �������
		RGB(0, 0, 0));			//����  ������

	osi_pen.CreatePen(			//������������ ���
		PS_SOLID,				//�������� �����
		3,						//������� 3 �������
		RGB(0, 0, 0));			//���� ������

	signal_pen.CreatePen(			//������ ��������� �������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(0, 0, 255));			//���� �����

	signal2_pen.CreatePen(			//������ ���������������� �������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(255, 0, 0));		//���� red


	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void Cnecorrtask1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		Pererisovka();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR Cnecorrtask1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Cnecorrtask1Dlg::Pererisovka()				//������� �����������
{

	PicDc->FillSolidRect(&Pic, RGB(250, 250, 250));					//���������� ��� 
	PicDcImp->FillSolidRect(&PicImp, RGB(250, 250, 250));			//���������� ��� 
	PicDcSvert->FillSolidRect(&PicSvert, RGB(250, 250, 250));		//���������� ���

	//������ �������

		//������� ����������
	xmin = -3;			//����������� �������� �
	xmax = t_sign - 1;			//������������ �������� �
	ymin = -0.5;			//����������� �������� y
	ymax = 6;		//������������ �������� y


	xp = ((double)(Pic.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(Pic.Height()) / (ymax - ymin));			//������������ ��������� ��������� �� �


	PicDc->SelectObject(&osi_pen);		//�������� ����

	//������ ��� Y
	PicDc->MoveTo(DOTS(0, ymax));
	PicDc->LineTo(DOTS(0, ymin));
	//������ ��� �
	PicDc->MoveTo(DOTS(xmin, 0));
	PicDc->LineTo(DOTS(xmax, 0));

	//������� ����
	PicDc->TextOutW(DOTS(2, ymax - 0.5), _T("S"));
	PicDc->TextOutW(DOTS(xmax - 3.5, 0.5), _T("t"));

	PicDc->SelectObject(&setka_pen);

	//��������� ����� �� �
	for (float x = 0; x <= xmax; x += xmax * 0.12)
	{
		PicDc->MoveTo(DOTS(x, ymax));
		PicDc->LineTo(DOTS(x, ymin));
	}
	//��������� ����� �� �
	for (float y = 0; y <= ymax; y++)
	{
		PicDc->MoveTo(DOTS(xmin, y));
		PicDc->LineTo(DOTS(xmax, y));
	}


	//������� ����� �� ���
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc->SelectObject(font);

	//�� Y � ����� 
	for (double i = 0; i <= ymax; i++)
	{
		CString str;
		str.Format(_T("%.0f"), i);
		PicDc->TextOutW(DOTS(-2, i), str);
	}
	//�� X � ����� 
	for (double j = xmax * 0.12; j <= xmax; j += xmax * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDc->TextOutW(DOTS(j, -0.2), str);
	}

	//������ ���+������

	//������� ����������
	xminimp = -3.5;			//����������� �������� �
	xmaximp = t_sign - 1;			//������������ �������� �
	yminimp = -0.015;			//����������� �������� y
	ymaximp = 0.13;		//������������ �������� y

	xpimp = ((double)(PicImp.Width()) / (xmaximp - xminimp));			//������������ ��������� ��������� �� �
	ypimp = -((double)(PicImp.Height()) / (ymaximp - yminimp));			//������������ ��������� ��������� �� �


	PicDcImp->SelectObject(&osi_pen);		//�������� ����

	//������ ��� Y
	PicDcImp->MoveTo(DOTSIMP(0, ymaximp));
	PicDcImp->LineTo(DOTSIMP(0, yminimp));
	//������ ��� �
	PicDcImp->MoveTo(DOTSIMP(xminimp, 0));
	PicDcImp->LineTo(DOTSIMP(xmaximp, 0));

	//������� ����
	PicDcImp->TextOutW(DOTSIMP(2, ymaximp - 0.01), _T("h"));
	PicDcImp->TextOutW(DOTSIMP(xmaximp - 3, 0.012), _T("t"));


	PicDcImp->SelectObject(&setka_pen);

	//��������� ����� �� �
	for (float x = 0; x <= xmaximp; x += xmaximp * 0.12)
	{
		PicDcImp->MoveTo(DOTSIMP(x, ymaximp));
		PicDcImp->LineTo(DOTSIMP(x, yminimp));
	}
	//��������� ����� �� �
	for (float y = ymaximp * 0.24; y <= ymaximp; y += ymaximp * 0.24)
	{
		PicDcImp->MoveTo(DOTSIMP(xminimp, y));
		PicDcImp->LineTo(DOTSIMP(xmaximp, y));
	}

	//������� ����� �� ���
	CFont fontsh;
	fontsh.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcImp->SelectObject(fontsh);

	//�� Y � ����� 
	for (double i = ymaximp * 0.12; i <= ymaximp; i += ymaximp * 0.12)
	{
		CString strsh;
		strsh.Format(_T("%.2f"), i);
		PicDcImp->TextOutW(DOTSIMP(-2.7, i), strsh);
	}
	//�� X � �����
	for (double j = 0; j <= xmaximp; j += xmaximp * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcImp->TextOutW(DOTSIMP(j, -0.005), str);
	}

	//������ svertki

	//������� ����������
	xminsvert = -3;			//����������� �������� �
	xmaxsvert = t_sign - 1;			//������������ �������� �
	yminsvert = -0.5;			//����������� �������� y
	ymaxsvert = 6;		//������������ �������� y

	xpsvert = ((double)(PicSvert.Width()) / (xmaxsvert - xminsvert));			//������������ ��������� ��������� �� �
	ypsvert = -((double)(PicSvert.Height()) / (ymaxsvert - yminsvert));			//������������ ��������� ��������� �� �


	PicDcSvert->SelectObject(&osi_pen);		//�������� ����

	//������ ��� Y
	PicDcSvert->MoveTo(DOTSSVERT(0, ymaxsvert));
	PicDcSvert->LineTo(DOTSSVERT(0, yminsvert));
	//������ ��� �
	PicDcSvert->MoveTo(DOTSSVERT(xminsvert, 0));
	PicDcSvert->LineTo(DOTSSVERT(xmaxsvert, 0));

	//������� ����
	PicDcSvert->TextOutW(DOTSSVERT(2, ymaxsvert - 0.5), _T("Y"));
	PicDcSvert->TextOutW(DOTSSVERT(xmaxsvert - 3.5, 0.5), _T("X"));


	PicDcSvert->SelectObject(&setka_pen);

	//��������� ����� �� x
	for (float x = 0; x <= xmaxsvert; x += xmaxsvert * 0.12)
	{
		PicDcSvert->MoveTo(DOTSSVERT(x, ymaxsvert));
		PicDcSvert->LineTo(DOTSSVERT(x, yminsvert));
	}
	//��������� ����� �� y
	for (float y = 0; y <= ymaxsvert; y++)
	{
		PicDcSvert->MoveTo(DOTSSVERT(xminsvert, y));
		PicDcSvert->LineTo(DOTSSVERT(xmaxsvert, y));
	}


	//������� ����� �� ���
	CFont fontsp;
	fontsp.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSvert->SelectObject(fontsp);

	//�� Y � ����� 
	for (double i = 0; i <= ymaxsvert; i++)
	{
		CString strsp;
		strsp.Format(_T("%.0f"), i);
		PicDcSvert->TextOutW(DOTSSVERT(-2, i), strsp);
	}
	//�� X � ����� 
	for (double j = xmaxsvert * 0.12; j <= xmaxsvert; j += xmaxsvert * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcSvert->TextOutW(DOTSSVERT(j, -0.2), str);
	}
}

void Cnecorrtask1Dlg::PererisovkaDC()
{
	PicDc->FillSolidRect(&Pic, RGB(250, 250, 250));			//���������� ��� 

	//������ �������

		//������� ����������
	xmin = -3;			//����������� �������� �
	xmax = t_sign - 1;			//������������ �������� �
	ymin = -0.5;			//����������� �������� y
	ymax = 6;		//������������ �������� y

	/*Mashtab(s, t_sign, ymin, ymax);*/

	xp = ((double)(Pic.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(Pic.Height()) / (ymax - ymin));			//������������ ��������� ��������� �� �


	PicDc->SelectObject(&osi_pen);		//�������� ����

	//������ ��� Y
	PicDc->MoveTo(DOTS(0, ymax));
	PicDc->LineTo(DOTS(0, ymin));
	//������ ��� �
	PicDc->MoveTo(DOTS(xmin, 0));
	PicDc->LineTo(DOTS(xmax, 0));

	//������� ����
	PicDc->TextOutW(DOTS(2, ymax - 0.5), _T("S"));
	PicDc->TextOutW(DOTS(xmax - 3.5, 0.5), _T("t"));


	PicDc->SelectObject(&setka_pen);

	//��������� ����� �� �
	for (float x = 0; x <= xmax; x += xmax * 0.12)
	{
		PicDc->MoveTo(DOTS(x, ymax));
		PicDc->LineTo(DOTS(x, ymin));
	}
	//��������� ����� �� �
	for (float y = 0; y <= ymax; y++)
	{
		PicDc->MoveTo(DOTS(xmin, y));
		PicDc->LineTo(DOTS(xmax, y));
	}


	//������� ����� �� ���
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc->SelectObject(font);

	//�� Y � ����� 
	for (double i = 0; i <= ymax; i++)
	{
		CString str;
		str.Format(_T("%.0f"), i);
		PicDc->TextOutW(DOTS(-2, i), str);
	}
	//�� X � ����� 
	for (double j = xmax * 0.12; j <= xmax; j += xmax * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDc->TextOutW(DOTS(j, -0.2), str);
	}
}

double Cnecorrtask1Dlg::signal(int t)			//������� ��� �������� �������
{
	double A[] = {A1_sign, A2_sign, A3_sign};
	double t0[] = {t01_sign, t02_sign, t03_sign};
	double del[] = {dis1_sign, dis2_sign, dis3_sign};
	double s = 0;

	for (int j = 0; j <= 2; j++)
	{
		s += A[j] * exp(-(((t - t0[j]) / del[j]) * ((t - t0[j]) / del[j])));
	}

	return s;
}

double Cnecorrtask1Dlg::Psi()		//������������ ��� ����
{
	float r = 0;
	for (int i = 1; i <= 12; i++)
	{
		r += ((rand() % 100) / (100 * 1.0) * 2) - 1;		// [-1;1]
	}
	return r / 12;
}

float Cnecorrtask1Dlg::function(float* x)			//���������� ��� ���
{
	// ��������� �������������� �������
	// ���������� �������� �������
	// ���������� ���������� �������� ������ ������, � ��������� ������ �������� ��������� �������
	float* xi = new float[t_sign];
	memset(xi, 0, t_sign * sizeof(float));

	float* sum = new float[t_sign];
	memset(sum, 0, t_sign * sizeof(float));

	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				sum[i] += x[j] * h_norm[t_sign + (i - j)];
			else
				sum[i] += x[j] * h_norm[i - j];
		}
	}

	for (int i = 0; i < t_sign; i++)
	{
		xi[i] = exp(-1 - sum[i]);
	}

	float Error = 0.;

	float* yi = new float[t_sign];
	memset(yi, 0, t_sign * sizeof(float));
	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				yi[i] += xi[j] * h_norm[t_sign + (i - j)];
			else
				yi[i] += xi[j] * h_norm[i - j];
		}
	}

	for (int i = 0; i < t_sign; i++)
	{
		Error += (svertka[i] - yi[i]) * (svertka[i] - yi[i]);
	}

	return Error;

	delete xi;
	delete yi;
	delete sum;
}

float Cnecorrtask1Dlg::MHJ(int kk, float* x)
{
	// kk - ���������� ����������; x - ������ ����������
	UpdateData(TRUE);
	float  TAU = 1.e-6f; // �������� ����������
	int i, j, bs, ps;
	float z, h, k, fi, fb;
	float* b = new float[kk];
	float* y = new float[kk];
	float* p = new float[kk];

	h = 1.;
	x[0] = 1.;
	for (i = 1; i < kk; i++)  x[i] = (float)rand() / RAND_MAX; // �������� ��������� �����������

	k = h;
	for (i = 0; i < kk; i++)	y[i] = p[i] = b[i] = x[i];
	fi = function(x);
	ps = 0;   bs = 1;  fb = fi;

	j = 0; char c;
	while (1)
	{
		//calc++; // ������� ��������. ����� ������������

		x[j] = y[j] + k;
		z = function(x);
		if (z >= fi) 
		{
			x[j] = y[j] - k;
			z = function(x);
			if (z < fi)   y[j] = x[j];
			else  x[j] = y[j];
		}
		else  y[j] = x[j];
		fi = function(x);

		if (j < kk - 1) { j++;  continue; }
		if (fi + 1e-8 >= fb) 
		{
			if (ps == 1 && bs == 0) 
			{
				for (i = 0; i < kk; i++) 
				{
					p[i] = y[i] = x[i] = b[i];
				}
				z = function(x);
				bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
				continue;
			}
			k /= 10.;
			if (k < TAU) break;
			j = 0;
			continue;
		}

		for (i = 0; i < kk; i++) {
			p[i] = 2 * y[i] - b[i];
			b[i] = y[i];
			x[i] = p[i];

			y[i] = x[i];
		}
		z = function(x);
		fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;

		float* vosst_sign = new float[t_sign];
		memset(vosst_sign, 0, t_sign * sizeof(float));

		float* sum = new float[t_sign];
		memset(sum, 0, t_sign * sizeof(float));

		for (int i = 0; i < t_sign; i++)
		{
			for (int j = 0; j < t_sign; j++)
			{
				if (i - j < 0)
					sum[i] += p[j] * h_norm[t_sign + (i - j)];
				else
					sum[i] += p[j] * h_norm[i - j];
			}
		}

		for (int i = 0; i < t_sign; i++)
		{
			vosst_sign[i] = exp(-1 - sum[i]);
		}

		PererisovkaDC();

		PicDc->SelectObject(&signal_pen);
		PicDc->MoveTo(DOTS(0, Signal[0]));
		
		for (int i = 0; i < t_sign; i++)
		{
			PicDc->LineTo(DOTS(i, Signal[i]));		//������ �������� ������
		}

		PicDc->SelectObject(&signal2_pen);
		PicDc->MoveTo(DOTS(0, vosst_sign[0]));
		ofstream dlout("res.txt");

		for (int i = 0; i < t_sign; i++)
		{
			PicDc->LineTo(DOTS(i, vosst_sign[i]));		//������ �������� ������
			dlout << i << "\t" << vosst_sign[i] << endl;
		}

		Sleep(50);

		dlout.close();

		CString err = NULL;
		err.Format(L"%.7f", fb);
		error = err;
		UpdateData(FALSE);

		if(_kbhit())
		{
			if ((c = _getch()) == 'p')
			{
				while (_getch() != 's');
			}
		}


		delete vosst_sign;
		delete sum;
	} //  end of while(1)

	for (i = 0; i < kk; i++)  x[i] = p[i];

	delete b;
	delete y;
	delete p;

	return fb;
}

void Cnecorrtask1Dlg::OnBnClickedButton1()			//���������� �������, ���������� �������������� � �������
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
	Pererisovka();
	
	float* ish_sign = new float[t_sign];
	float* h_sign = new float[t_sign];
	
	float norm = 0.;

	for (int i = 0; i < t_sign; i++)
	{
		ish_sign[i] = signal(i);	//������� �������� ������
		h_sign[i] = A1_imp * exp(-(((i - t01_imp) / dis1_imp) * ((i - t01_imp) / dis1_imp))) +
					A2_imp * exp(-(((i - t02_imp) / dis2_imp) * ((i - t02_imp) / dis2_imp)));
		norm += h_sign[i];
	}	

	for (int i = 0; i < t_sign; i++)
	{
		h_norm[i] = h_sign[i]/norm;
		Signal[i] = ish_sign[i];
	}

	memset(svertka, 0, t_sign * sizeof(float));
	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				svertka[i] += ish_sign[j] * h_norm[t_sign + (i - j)];
			else 
				svertka[i] += ish_sign[j] * h_norm[i - j];
		}
	}

	PicDc->SelectObject(&signal_pen);
	PicDcImp->SelectObject(&signal_pen);
	PicDcSvert->SelectObject(&signal_pen);
	PicDc->MoveTo(DOTS(0, ish_sign[0]));
	PicDcImp->MoveTo(DOTSIMP(0, h_norm[0]));
	PicDcSvert->MoveTo(DOTSSVERT(0, svertka[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDc->LineTo(DOTS(i, ish_sign[i]));		//������ �������� ������
		PicDcImp->LineTo(DOTSIMP(i, h_norm[i]));
		PicDcSvert->LineTo(DOTSSVERT(i, svertka[i]));
	}

	delete ish_sign;
	delete h_sign;
	}

void Cnecorrtask1Dlg::OnBnClickedButton3()				//�������������� ��������� �������
{
	float Error_kvadr = 0;

	float* lyambda = new float[t_sign];
	memset(lyambda, 0, t_sign * sizeof(float));

	Error_kvadr = MHJ(t_sign, lyambda);

	delete lyambda;
}

void Cnecorrtask1Dlg::OnBnClickedButton2()		//���������� � ����� �������, ���������� �������������� � �������
{
	UpdateData(TRUE);
	Pererisovka();

	float* ish_sign = new float[t_sign];
	float* h_sign = new float[t_sign];

	float norm = 0.;

	for (int i = 0; i < t_sign; i++)
	{
		ish_sign[i] = signal(i);	//������� �������� ������
		h_sign[i] = A1_imp * exp(-(((i - t01_imp) / dis1_imp) * ((i - t01_imp) / dis1_imp))) +
			A2_imp * exp(-(((i - t02_imp) / dis2_imp) * ((i - t02_imp) / dis2_imp)));
		norm += h_sign[i];
	}

	for (int i = 0; i < t_sign; i++)
	{
		h_norm[i] = h_sign[i] / norm;
		Signal[i] = ish_sign[i];
	}

	memset(svertka, 0, t_sign * sizeof(float));
	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				svertka[i] += ish_sign[j] * h_norm[t_sign + (i - j)];
			else
				svertka[i] += ish_sign[j] * h_norm[i - j];
		}
	}

	//��������� ����

	double d = percent_shum / 100.;

	double energ_signal = 0;
	for (int t = 0; t < t_sign; t++)
	{
		energ_signal += svertka[t] * svertka[t];
	}

	double* psi = new double[t_sign];
	memset(psi, 0, t_sign * sizeof(double));

	for (int t = 0; t < t_sign; t++)
	{
		psi[t] = Psi();
	}

	double qpsi = 0;
	for (int t = 0; t < t_sign; t++)
	{
		qpsi += psi[t] * psi[t];
	}

	double alpha = sqrt(d * energ_signal / qpsi);

	double* Shum = new double[t_sign];
	memset(Shum, 0, t_sign * sizeof(double));

	for (int i = 0; i < t_sign; i++)
	{
		Shum[i] = svertka[i] + alpha * psi[i];
	}

	for (int i = 0; i < t_sign; i++)
	{
		svertka[i] = Shum[i];
	}


	PicDc->SelectObject(&signal_pen);
	PicDcImp->SelectObject(&signal_pen);
	PicDcSvert->SelectObject(&signal_pen);
	PicDc->MoveTo(DOTS(0, Signal[0]));
	PicDcImp->MoveTo(DOTSIMP(0, h_norm[0]));
	PicDcSvert->MoveTo(DOTSSVERT(0, svertka[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDc->LineTo(DOTS(i, Signal[i]));		//������ �������� ������
		PicDcImp->LineTo(DOTSIMP(i, h_norm[i]));
		PicDcSvert->LineTo(DOTSSVERT(i, svertka[i]));
	}

	delete Shum;
	delete psi;
}

void Cnecorrtask1Dlg::OnBnClickedButton4()				//�������������� ��������� �������
{
	float Error_kvadr = 0;

	float* lyambda = new float[t_sign];
	memset(lyambda, 0, t_sign * sizeof(float));

	Error_kvadr = MHJ(t_sign, lyambda);

	delete lyambda;
}