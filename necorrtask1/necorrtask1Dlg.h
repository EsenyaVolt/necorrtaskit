
// necorrtask1Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� Cnecorrtask1Dlg
class Cnecorrtask1Dlg : public CDialogEx
{
// ��������
public:
	Cnecorrtask1Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_NECORRTASK1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//������� ���������
	CWnd * PicWnd;
	CDC * PicDc;
	CRect Pic;

	CWnd * PicWndImp;
	CDC * PicDcImp;
	CRect PicImp;

	CWnd * PicWndSvert;
	CDC * PicDcSvert;
	CRect PicSvert;


	//���������� ��� ������ � ���������
	double xp = 0, yp = 0,			//����������� ���������
		xmin = 0, xmax = 0,			//�������������� � ����������� �������� � 
		ymin = 0, ymax = 0;			//�������������� � ����������� �������� y

	double xpimp = 0, ypimp = 0,			//����������� ���������
		xminimp = 0, xmaximp = 0,			//�������������� � ����������� �������� � 
		yminimp = 0, ymaximp = 0;			//�������������� � ����������� �������� y

	double xpsvert = 0, ypsvert = 0,			//����������� ���������
		xminsvert = 0, xmaxsvert = 0,			//�������������� � ����������� �������� � 
		yminsvert = 0, ymaxsvert = 0;			//�������������� � ����������� �������� y

	//���������� �����
	CPen osi_pen;		// ����� ��� ����
	CPen setka_pen;		// ��� �����
	CPen signal_pen;		// ��� ������� �������
	CPen signal2_pen;

public:
	double A1_sign;
	double A2_sign;
	double A3_sign;
	double A1_imp;
	double A2_imp;
	double dis1_sign;
	double dis2_sign;
	double dis3_sign;
	double t01_sign;
	double t02_sign;
	double t03_sign;
	double dis1_imp;
	double dis2_imp;
	double t01_imp;
	double t02_imp;
	int t_sign;

	float* svertka = new float[t_sign];			//���������� ������ ��� �������
	float* h_norm = new float[t_sign];			//���������� ������ ��� ������������� ���.��������������
	float* Signal = new float[t_sign];			//���������� ������ ��� �������

	//����������� ������� �� ������
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();

	afx_msg double signal(int t);			//������� �������
	afx_msg float MHJ(int kk, float* x);	//����� ����-������
	afx_msg float function(float* x);		//����������
	afx_msg void Pererisovka();				//������� ����������� �������
	afx_msg void PererisovkaDC();			//������� ����������� DC
	afx_msg double Psi();					//������������ ��� ����

	CString error;							//���������� ����� ���������
	int percent_shum;						//��� � ���������
};
